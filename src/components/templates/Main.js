import React from "react";
import { AddCost } from "../AddCost";
import { HistoryCost } from "../HistoryCost";

export const Main = () => {
	return (
		<main className="container">
			<div>
				<h1>Control gastos</h1>
			</div>
			<section className="row">
				<article className="col-6">
					<AddCost all_cost={all_cost}/>
				</article>
				<article className="col-6" >
					<HistoryCost all_cost={all_cost}/>
				</article>
			</section>
		</main>
	);
};

let all_cost = [];
