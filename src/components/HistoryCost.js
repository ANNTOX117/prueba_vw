import React, { useState } from "react";
export const HistoryCost = ({all_cost}) => {
	return (
		<>
			<FindCost all_cost={all_cost}/>
		</>
	);
};
const FindCost = ({all_cost}) => {
	const [date_find, setDate_find] = useState();
	const [show, setShow] = useState(false);
	const [data, setData] = useState([]);
	function handleSubmit(e,date_find,all_cost){
		let data_finded = [];
		e.preventDefault();
		all_cost.map(e=>{
			if (e.date === date_find) {
				data_finded.push(e)
			}
		})
		setShow(true);
		setData(data_finded);
	}
	return (
		<>
			<div className="row">
				<form className="row g-2" onSubmit={(e)=>handleSubmit(e,date_find,all_cost)}>
					<div className="col">
						<input
							type="date"
							name="date_to_find"
							className="form-control"
							placeholder="dd-mm-yyyy"
							value={date_find}
							onChange={(e) => setDate_find(e.target.value)}
							required
							id="date_to_find"
						/>
					</div>
					<div className="col">
						<button type="submit" className="btn btn-primary mb-3">
							Buscar
						</button>
					</div>
				</form>
			</div>
			{show?<ShowCostByDate data_finded={data}/>:false}
		</>
	);
};

const ShowCostByDate = ({data_finded}) => {
	const datos = [];
	let total = 0;
	data_finded.map((e,index) => {
		datos.push(
			<tr key={index}>
				<td>{e.title}</td>
				<td>{e.costo}</td>
			</tr>
		);
		total += parseFloat(e.costo);
	});

	return (
		<>
			<div className="card">
				<div className="card-header text-center">
					<span>Historial de gastos</span>
				</div>
				<div className="card-body">
					<table className="table text-center">
						<thead>
							<tr></tr>
						</thead>
						<tbody>{datos}</tbody>
					</table>
				</div>
			</div>
			<div className="text-danger">
				<p>{(total === 0)?"no hay gastos":`El total de gastos es: $${total}`}</p>
			</div>
		</>
	);
};
