import React, { useState } from "react";
export const AddCost = ({all_cost}) => {
	const [title, setTitle] = useState("");
	const [cost, setCost] = useState("");
	const [date, setDate] = useState();
	function handleChange(e, type) {
		let value = e.target.value;
		if (type) {
			let regex = /^[a-zA-Z ]*$/;
			if (value.match(regex) !== null) setTitle(value);
		} else {
			let regex = /(\d+(?:\.\d+)?)/;
			if (value.match(regex) !== null) setCost(value);
		}
	}

	function handleSubmit(e) {
		e.preventDefault();
		let costo = parseFloat(cost)
		let element = {
			title,
			costo,
			date 
		}
		all_cost.push(element);
		alert("costo almacenado");
		setTitle("");
		setDate("");
		setCost("");
	}
	return (
		<div className="card">
			<div className="card-body">
				<form onSubmit={(e)=>handleSubmit(e)}>
					<div className="mb-3">
						<input
							type="text"
							className="form-control"
							id="tittle"
							placeholder="Introduzca gasto"
							value={title}
							name="title"
							onChange={(e) => handleChange(e, true)}
							required
						/>
					</div>
					<div className="input-group mb-3">
						<span className="input-group-text">$</span>
						<input
							type="text"
							className="form-control"
							placeholder="Digite el gasto"
							name="cost"
							value={cost}
							onChange={(e) => handleChange(e, false)}
							required
						/>
					</div>
					<div className="mb-3">
						<input
							type="date"
							name="begin"
							className="form-control"
							placeholder="dd-mm-yyyy"
							value={date}
							onChange={(e) => setDate(e.target.value)}
							required
						/>
					</div>
					<button type="submit" className="btn btn-primary">
						Subir
					</button>
				</form>
			</div>
		</div>
	);
};
