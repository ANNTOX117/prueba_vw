import Header from "./components/templates/Header";
import { Main } from "./components/templates/Main";
function App() {
	return (
		<div className="App">
			<Header title="Control gastos" />
			<Main />
		</div>
	);
}

export default App;
